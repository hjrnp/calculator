<?php
error_reporting(E_ALL);
require_once("db_class.php");

// DB -> Stores all DB connections
// -- Calc -> Stores connections specific to calc functionality, in future can
// -- easily expand to allow different type of connections for different purposes
// -- -- Current -> The currently active Calc db - can in future allow dynamic switching
// -- -- based on connection situation.
$GLOBALS["db"]["calc"]["current"] = new db_manager([
  "db_driver" => "mysql",
  "db_host" => "localhost",
  "db_name" => "",
  "db_user" => "",
  "db_pass" => "",
  "db_charset" => 'utf8'
]);
