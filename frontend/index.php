<?php
// Initially building index.php as a procedrual, simply because it's faster.
// However, designing it so it can later easily be switched to an OOP design.

// An array showing which of our snippets to include
// Pulls from snippets folder.
// Using this, if later expanded, we could easily design entirely
// new page layouts by loading a different array.
$display_array = [
  "header",
  "calc_form",
  "results",
  "footer"
];

// page_display is a global that gets built progressively by the snippets
// By the snippets appending, instead of outright echoing, we can easily later
// convert them to classes, chaning the front to something more object oriented,
// Set up routing controls, or set header values or other global variables
// inside of our snippets.
$GLOBALS["page_display"] = "";


$page_array = [];

// requiring each snippet
foreach ($display_array as $snippet){
  require_once("snippets/" . $snippet . ".php");
}

echo $GLOBALS["page_display"];
