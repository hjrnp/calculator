function update_calc(){
  let chosen_calc = $("#calc_types").val();
  var post_data = {
    "func": "get_current",
    "values": {
      "value1": null,
      "value2": null,
      "calculator": chosen_calc
    }
  };
  var ajaxed = $.post( "http://joshup.com/experiments/calculator/backend/index.php", post_data, function(data) {
    console.log(data);
  })
    .done(function(data) {
      var updates = JSON.parse(data);
      $('#calc_types').empty();
      for(let i=0, len=updates.calc_types.length; i < len; i++){
        var option = new Option(updates.calc_types[i], updates.calc_types[i]);
        $(option).html(updates.calc_types[i]);
        // Calc turn-off altering to prevent double-firing.
        $("#calc_types").append(option);
      }
      $('#func_types').empty();
      for(let i=0, len=updates.func_types.length; i < len; i++){
        var option = new Option(updates.func_types[i], updates.func_types[i]);
        $(option).html(updates.func_types[i]);
        $("#func_types").append(option);
      }
      $('#history').empty();
      for(let i=0, len=updates.history.length; i < len; i++){
        $("#history").append("<pre>(" + updates.history[i].value1 + " " + updates.history[i].func + " " + updates.history[i].value2 + ") = " +  updates.history[i].result + "</pre>");
      }
    })
    .fail(function(data) {
      alert( "Action could not be performed" );
      console.log("Error");
      console.log(data);
    });
}

// Sending Functions
function change_calc_type(){
  update_calc();
}

function send_func(){
  let chosen_calc = $("#calc_types").val();
  let func = $("#func_types").val();
  let val1 = $("#value1").val();
  let val2 = $("#value2").val();
  post_data = {
    "func": func,
    "values": {
      "value1": val1,
      "value2": val2,
      "calculator": chosen_calc
    }
  };
  var ajaxed = $.post( "http://joshup.com/experiments/calculator/backend/index.php", post_data, function(data) {
    console.log(data);
  })
    .done(function(data) {
      console.log("Done");
    })
    .fail(function(data) {
      alert( "Calculation could not be performed" );
      console.log("Error");
    });

  // Good spot to put a waiting dialog if needed...

  // Fire off the calc updater, regardless of results.
  ajaxed.always(function() {
    update_calc();
  });
}

$( document ).ready(function() {
    update_calc();
    $("#change_calculator").click(function(event){
      event.preventDefault();
      change_calc_type();
    });

    $("#run_calculation").click(function(event){
      event.preventDefault();
      send_func();
    });
});
