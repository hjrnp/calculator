<?php

require_once("../configs/config.php");
class Restful_Interface {
  private $function_array;
  private $option_array;
  private $db;
  private $user_id;
  private $active_function;
  public function __construct($initData=[]){
    // print_r($dec_post);
    $this -> function_array = [
      "basic" => [
        "add",
        "divide",
        "multiply",
        "subtract"
      ],
      "scientific" => [
        "abs",
        "add",
        "divide",
        "multiply",
        "subtract"
      ]
    ];
    $this -> db = $GLOBALS["db"]["calc"]["current"];
    //Set Initial Variables
    $this -> active_function = $_POST["func"];
    $this -> option_array = $_POST["values"];

    // See if it's an existing user
    if (isset($_COOKIE["user_id"])){
      $this -> user_id = $_COOKIE["user_id"];
    } else {
      $this -> user_id = $this -> db -> new_user();
    }
    return;
  }

  public function history(){
    $this -> db -> user_history();
  }

  // The standard function for just doing a math function
  public function do_math(){
    $the_array = $this -> function_array;
    $the_function = $this -> active_function;
    $option_array = $this -> option_array;
    $the_db = $this -> db;
    if ($the_function != "get_current"){
      print_r($_POST);
    }
    // Check if mathmatic function is valid
    if (!is_string($the_function)){
      return("Invalid Function");
    }
    // Check if we've got a class function for the math function...
    if (method_exists($this, $the_function)){

      $result = call_user_func([$this, $the_function]);
      $insert_user_array = ["value1" => $option_array["value1"], "func" => $the_function, "value2" => $option_array["value2"], "result" => $result, "calculator" => $option_array["calculator"]];
      $the_db -> insert_calc($insert_user_array);
      return $result;
    } else {
      // If we don't have a class function for the math function, check if the
      // math function exists among existing allowed math functions.
      if (isset($this -> function_array[$the_function]) || array_key_exists($this -> function_array[$the_function])){
        $result = $the_function($option_array);
        $the_db -> insert_calc(["value1" => $option_array["value1"], "func" => $the_function, "value2" => $option_array["value2"], "result" => $result, "calculator" => $option_array["calculator"]]);
        return $result;
      } else {
        return("Is an Inaccessible Function");
      }
    }
  }

  // Calculator set type functions, updating DB takes care of the work, just
  // need the function name.
  public function change_calc($type){
    $option_array = $this -> option_array;
    return $option_array["calculator"];
  }

  // Built in math functions
  public function add($option_array){
    $option_array = $this -> option_array;
    $result = floatval($option_array["value1"]) + floatval($option_array["value2"]);
    return $result;
  }

  public function subtract($option_array){
    $option_array = $this -> option_array;
    $result = floatval($option_array["value1"]) - floatval($option_array["value2"]);
    return $result;
  }

  public function multiply($option_array){
    $option_array = $this -> option_array;
    $result = floatval($option_array["value1"]) * floatval($option_array["value2"]);
    return $result;
  }

  public function divide($option_array){
    $option_array = $this -> option_array;
    $result = floatval($option_array["value1"]) / floatval($option_array["value2"]);
    return $result;
  }

  // Gets what the current values are.
  public function get_current(){
    $my_db = $this -> db;
    $option_array = $this -> option_array;
    $function_array = $this -> function_array;
    // Default is basic if no calculator active
    $last_calc = $option_array["calculator"];
    if ($last_calc == "" || $last_calc == NULL){
      $active_calc = "basic";
    } else {
      $active_calc = $last_calc;
    }
    $return_array = [
      "calc_types" => array_keys($function_array),
      "func_types" => $function_array[$active_calc],
      "value1" => $my_db -> get_last_value(),
      "history" => $my_db -> get_result_list()
    ];
    return $return_array;
  }

  public function send_output(){
    $the_output = $this -> do_math();
    $troubleshooting = array_merge(["Troubleshooting_Data" => $_POST], ["Option Array" => $last_calc]);
    $the_output = array_merge($the_output, $troubleshooting);
    return (json_encode($the_output));
  }

}
