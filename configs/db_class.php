<?php

class db_manager {
  private $my_pdo;
  private $connection_data;
  private $current_user;
  public function __construct(array $initData = []) {
        $this -> conection_data = $initData;
        $this -> my_pdo = $this -> start_pdo_connection();
        if (isset($_COOKIE["user_id"])){
          $this -> current_user = intval($_COOKIE["user_id"]);
          setcookie("user_id", $_COOKIE["user_id"],  time()+60*60*24*60);
        } else {
          $this -> current_user = $this -> new_user();
          $_COOKIE["user_id"] = $this -> new_user();
          setcookie("user_id", $_COOKIE["user_id"],  time()+60*60*24*60);
        }
  }

  public function start_pdo_connection(){
    $db_data = $this -> conection_data;
    $connection = $db_data["db_driver"] . ':dbname=' . $db_data["db_name"] . ';host=' . $db_data["db_host"] . ";charset=" . $db_data["db_charset"];
    //Set Initial Variables
    try{
      return new PDO( $connection, $db_data["db_user"], $db_data["db_pass"], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
    }
    catch(PDOException $ex){
        echo(json_encode(array('outcome' => false, 'message' => 'Unable to connect' . $ex)));
        return;
    }
  }

  public function new_user(){
    $rand = rand();
    while ($this -> get_user($rand) != null){
      $rand = rand();
    }
    return $rand;
  }

  public function get_user($user_id){
    $this_pdo = $this -> my_pdo;
    $query = $this_pdo -> prepare("SELECT `user_id` FROM `calculations` WHERE `user_id` = ? LIMIT 1");
    $query->execute([$user_id]);
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return $value = $row["user_id"];
  }

  public function get_last_calculator(){
    $this_pdo = $this -> my_pdo;
    $query = $this_pdo -> prepare("SELECT `calculator` FROM `calculations` WHERE (`user_id` = ?) ORDER BY time DESC LIMIT 1 ");
    $query->execute([$this -> current_user]);
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return $value = $row["calculator"];
  }

  public function get_last_value(){
    $this_pdo = $this -> my_pdo;
    $query = $this_pdo -> prepare("SELECT `result` FROM `calculations` WHERE (`user_id` = ? AND `func` <> 'change_calc' AND `func` <> 'get_current') ORDER BY time DESC LIMIT 1");
    $query->execute([$this -> current_user]);
    $row = $query->fetch(PDO::FETCH_ASSOC);
    return $value = $row["result"];
  }

  public function get_result_list(){
    $this_pdo = $this -> my_pdo;
    $query = $this_pdo -> prepare("SELECT `value1`, `func`, `value2`, `result` FROM `calculations` WHERE (`user_id` = ? AND `func` <> 'get_current') ORDER BY time DESC");
    $query->execute([$this -> current_user]);
    $history = $query->fetchall();
    return $history;
  }

  public function insert_calc($array){
    $this_pdo = $this -> my_pdo;
    $query = $this_pdo -> prepare("INSERT INTO `calculations`(`user_id`, `value1`, `func`, `value2`, `result`, `calculator`) VALUES (?, ?, ?, ?, ?, ?) ");
    $query->execute([$this -> current_user, $array["value1"], $array["func"], $array["value2"], $array["result"], $array["calculator"]]);
    return;
  }
}
