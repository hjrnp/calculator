<?php
$GLOBALS["page_display"] .= <<< HTML
  <!doctype html>

  <html lang="en">
  <head>
    <meta charset="utf-8">

    <title>Josh Calc</title>
    <meta name="description" content="Josh Calc">
HTML;
    // Make sure the latest css is always loaded.
    $GLOBALS["page_display"] .= '<link rel="stylesheet" href="css/styles.css?v=' . filemtime(dirname(__DIR__) . "/css/styles.css") . '">';

$GLOBALS["page_display"] .= <<< HTML
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
HTML;

// Make sure the latest script is always loaded.
$GLOBALS["page_display"] .= '<script src="js/calc.js?v=' . filemtime(dirname(__DIR__) . "/js/calc.js") . '"></script>';

$GLOBALS["page_display"] .= <<< HTML
  </head>
  <body>

HTML;
